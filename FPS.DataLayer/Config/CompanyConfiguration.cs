﻿using FPS.DataLayer.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.DataLayer.Config
{
    public class CompanyConfiguration : IEntityTypeConfiguration<Company>
    {
        /*public CompanyConfigurations()
        {
            this.Property(s => s.CompanytName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(s => s.IsD)
                .IsConcurrencyToken();

            // Configure a one-to-one relationship between Student & StudentAddress
            this.HasOptional(s => s.Address) // Mark Student.Address property optional (nullable)
                .WithRequired(ad => ad.Student); // Mark StudentAddress.Student property as required (NotNull).
        }*/
        public void Configure(EntityTypeBuilder<Company> builder)
        {
            //throw new NotImplementedException();
            builder.ToTable(nameof(Company));
            builder.HasKey(t => t.Id);
            //builder.Property(t => t.Id).ValueGeneratedOnAdd();
            builder.Property(t => t.IsDeleted).HasDefaultValue(false);
        }
    }
}
