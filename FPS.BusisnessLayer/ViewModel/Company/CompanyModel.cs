﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.BusisnessLayer.ViewModel.Company
{
    public class CompanyModel : BaseModel
    {
        public string CompanyName { get; set; }
        public string CompanyDescription { get; set;}
    }
}
