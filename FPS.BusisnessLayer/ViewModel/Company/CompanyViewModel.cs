﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.BusisnessLayer.ViewModel.Company
{
    public class CompanyViewModel
    {
        public Guid Id { get; set; }
        public string? CompanyName { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        //public string? CreatedByName { get; set; }
    }
}
