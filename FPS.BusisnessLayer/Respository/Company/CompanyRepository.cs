﻿using FPS.BusisnessLayer.ViewModel.Company;
using FPS.DataLayer.Context;
using FPS.DataLayer.Entity;

//using FPS.DataLayer.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.BusisnessLayer.Respository.Company
{
    public class CompanyRepository : BaseRepository, ICompanyRepository
    {
        public CompanyRepository(FPSContext context) : base(context)
        {
        }

        public void AddCompany(CompanyModel model)
        {
            //throw new NotImplementedException();
            FPS.DataLayer.Entity.Company company = new DataLayer.Entity.Company();
            company.Id = Guid.NewGuid();
            company.CompanyName = model.CompanyName;
            company.CreatedDate = DateTime.Now;
            company.UpdatedDate = DateTime.Now;
            company.CreatedBy = model.CreatedBy;
            company.UpdatedBy = model.UpdatedBy;
            company.IsDeleted = false;
            _context.Companies.Add(company);
            _context.SaveChanges();
        }

        public void DeleteCompany(Guid companyId)
        {
            //throw new NotImplementedException();
            var deleteCompany = _context.Companies.FirstOrDefault(c => c.Id == companyId);
            if (deleteCompany != null)
            {
                deleteCompany.IsDeleted = true;
                _context.SaveChanges();
            }
        }

        public List<FPS.DataLayer.Entity.Company> GetAll()
        {
            //throw new NotImplementedException();
            return _context.Companies.Where(c => !c.IsDeleted)
                                    .ToList();
        }

        public CompanyViewModel GetByCompanyId(Guid companyId)
        {
            //throw new NotImplementedException();
            var company = _context.Companies.Where(c => !c.IsDeleted)
                                            .FirstOrDefault(c => c.Id == companyId);
            if (company != null)
            {
                return new CompanyViewModel()
                {
                    CompanyName = company.CompanyName,
                    CreatedDate = company.CreatedDate,
                    UpdatedDate = company.UpdatedDate
                };
            }
            return null;
        }

        public void UpdateCompany(Guid companyId, CompanyModel model)
        {
            //throw new NotImplementedException();
            var updateCompany = _context.Companies.FirstOrDefault(c => c.Id == companyId);
            if (updateCompany != null)
            {
                updateCompany.CompanyName = model.CompanyName;
                updateCompany.CreatedDate = DateTime.Now;
                updateCompany.UpdatedDate = DateTime.Now;
                updateCompany.CreatedBy = model.CreatedBy;
                updateCompany.UpdatedBy = model.UpdatedBy;
                _context.Companies.Update(updateCompany);
                _context.SaveChanges();
            }
        }
    }
}
