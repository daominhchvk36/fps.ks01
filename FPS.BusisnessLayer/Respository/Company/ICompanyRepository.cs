﻿using FPS.BusisnessLayer.ViewModel.Company;
using FPS.DataLayer.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.BusisnessLayer.Respository.Company
{
    public interface ICompanyRepository
    {
        public void AddCompany(CompanyModel model);
        public void UpdateCompany(Guid companyId, CompanyModel model);
        public void DeleteCompany(Guid companyId);
        public List<FPS.DataLayer.Entity.Company> GetAll();
        public CompanyViewModel GetByCompanyId(Guid companyId);
    }
}
