﻿using FPS.BusisnessLayer.Respository.Company;
using FPS.BusisnessLayer.ViewModel.Company;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FPS.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyController : ControllerBase
    {
        public readonly ICompanyRepository _companyRepository;

        public CompanyController(ICompanyRepository companyRepository)
        {
            _companyRepository = companyRepository;
        }

        [HttpPost]
        [Route("AddCompany")]
        public void AddCompany(CompanyModel model)
        {
            _companyRepository.AddCompany(model);
        }

        [HttpPost]
        [Route ("UpdateCompany")]
        public void UpdateCompany(Guid companyId, CompanyModel model)
        {
            _companyRepository.UpdateCompany(companyId, model);
        }

        [HttpPost]
        [Route("DeleteCompany")]
        public void DeleteCompany(Guid companyId)
        {
            _companyRepository.DeleteCompany(companyId);
        }

        [HttpGet]
        [Route("GetByCompanyId")]
        public CompanyViewModel GetByCompanyId(Guid companyId)
        {
            return _companyRepository.GetByCompanyId(companyId);
        }

        [HttpGet]
        [Route("GetAll")]

        public List<FPS.DataLayer.Entity.Company> GetAll()
        {
            return _companyRepository.GetAll();
        }
    }
}
