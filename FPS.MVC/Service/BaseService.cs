﻿namespace FPS.MVC.Service
{
    public class BaseService
    {
        public readonly HttpClient _httpClient;
        public BaseService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }
    }
}
